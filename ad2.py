import os
from typing import Union, Optional, List, Tuple, Dict, cast
from numbers import Real
from enum import Enum, auto
import networkx as nx

EXPRESSION_COUNTER = 0


class Ops(Enum):
    ADD = auto()  # +
    SUB = auto()  # -
    MUL = auto()  # *
    DIV = auto()  # /
    SIN = auto()  # sin
    COS = auto()  # cos
    COL = auto()  # collection

    @property
    def is_ac(self) -> bool:
        return self == self.ADD or self == self.MUL or self == self.COL  # type: ignore


class Trace:
    _fields: Union[Tuple[Ops, List['Trace'], Optional[str]], str, Real]

    def __init__(self, name=EXPRESSION_COUNTER):
        global EXPRESSION_COUNTER
        EXPRESSION_COUNTER += 1
        if name is EXPRESSION_COUNTER:
            name = str(name)
        self._fields = name

    @property
    def istree(self) -> bool:
        return isinstance(self._fields, tuple)

    @property
    def is_const(a) -> bool:
        return (not a.istree) and (not isinstance(a.name, str))

    @property
    def name(self) -> Union[Optional[str], Real]:
        if self.istree:
            return self.tree[2]
        else:
            return cast(Union[str, Real], self._fields)

    @name.setter
    def name(self, name: str) -> 'Trace':
        if self.istree:
            ff = cast(Tuple[Ops, List['Trace'], Optional[str]], self._fields)
            ll = list(ff)
            ll[2] = name
            self._fields = cast(Tuple[Ops, List['Trace'], Optional[str]],
                                tuple(ll))
        else:
            self._fields = name
        return self

    @property
    def tree(self) -> Tuple[Ops, List['Trace'], Optional[str]]:
        if self.istree:
            return cast(Tuple[Ops, List['Trace'], Optional[str]], self._fields)
        else:
            raise Exception(f"{self} is not a leaf node!")

    @property
    def f(self) -> Ops:
        return self.tree[0]

    @property
    def args(self) -> List['Trace']:
        return self.tree[1]

    def graph(self):
        G = nx.DiGraph()

        # post-order

        def _vis(v, v_to_id):
            nid = v_to_id.get(v, None)
            if v.istree and nid is None:
                for arg in v.args:
                    _vis(arg, v_to_id)

            if nid is None:
                nid = v_to_id[v] = len(v_to_id)
            else:
                return

            if v.istree:
                f = v.f
                label = f"{f} ({nid})"
                if v.name is not None:
                    label = v.name + "\n" + label
                G.add_node(nid, label=label)
                for i, arg in enumerate(v.args):
                    if f.is_ac or len(v.args) <= 1:
                        G.add_edge(v_to_id.get(arg), nid)
                    else:
                        G.add_edge(v_to_id.get(arg), nid, label=i + 1)
            else:
                G.add_node(nid, label=f"{v.name} ({nid})")
            return

        v_to_id: Dict['Trace', int] = dict()
        _vis(self, v_to_id)
        return G, v_to_id

    def reverse_ad(self) -> 'Trace':
        vs = set()
        arr_args = []

        def _vis(v, v_to_d):
            d = v_to_d.get(v, None)
            if d is None:
                d = Trace(1)
                v_to_d[v] = d
            if v.istree:
                f = v.f
                args = v.args
                # TODO: clean up
                if f == Ops.ADD:
                    for arg in args:
                        v_to_d[arg] = d + v_to_d.get(arg, None)
                elif f == Ops.SUB and len(args) == 1:
                    b, = args
                    v_to_d[b] = -d + v_to_d.get(b, None)
                elif f == Ops.SUB:
                    assert (len(args) == 2)
                    a, b = args
                    v_to_d[a] = d + v_to_d.get(a, None)
                    v_to_d[b] = -d + v_to_d.get(b, None)
                elif f == Ops.MUL:
                    assert (len(args) == 2)
                    a, b = args
                    v_to_d[a] = d * b + v_to_d.get(a, None)
                    v_to_d[b] = d * a + v_to_d.get(b, None)
                elif f == Ops.DIV:
                    assert (len(args) == 2)
                    a, b = args
                    v_to_d[a] = d * (Trace(1) / b) + v_to_d.get(a, None)
                    v_to_d[b] = d * (-v / b) + v_to_d.get(b, None)
                elif f == Ops.COS:
                    assert (len(args) == 1)
                    a, = args
                    v_to_d[a] = d * (-sin(a)) + v_to_d.get(a, None)
                elif f == Ops.SIN:
                    assert (len(args) == 1)
                    a, = args
                    v_to_d[a] = d * (cos(a)) + v_to_d.get(a, None)
                else:
                    raise Exception(f"{f} is not handled")
                for arg in args:
                    _vis(arg, v_to_d)
            else:
                if v.is_const or v in vs:
                    return
                vs.add(v)
                d.name = "∂" + v.name
                arr_args.append(d)

        v_to_d: Dict['Trace', 'Trace'] = dict()
        _vis(self, v_to_d)
        return v.similar(Ops.COL, arr_args)

    # Tracing
    def similar(self, f, args) -> 'Trace':
        x = Trace()
        x._fields = (f, args, None)
        return x

    def __add__(self, b) -> 'Trace':
        if b is None:
            return self
        else:
            return self.similar(Ops.ADD, [self, b])

    def __sub__(self, b) -> 'Trace':
        return self.similar(Ops.SUB, [self, b])

    def __neg__(self) -> 'Trace':
        return self.similar(Ops.SUB, [self])

    def __mul__(self, b) -> 'Trace':
        return _mul(self, b)

    def __rmul__(self, b) -> 'Trace':
        return _mul(b, self)

    def __truediv__(self, b) -> 'Trace':
        return self.similar(Ops.DIV, [self, b])


def _mul(a, b) -> Trace:
    if not isinstance(a, Trace):
        a = Trace(a)
    if not isinstance(b, Trace):
        b = Trace(b)
    if not b.istree and b.name == 1:
        return a
    elif not a.istree and a.name == 1:
        return b
    else:
        return a.similar(Ops.MUL, [a, b])


def sin(self: Trace) -> Trace:
    return self.similar(Ops.SIN, [self])


def cos(self: Trace) -> Trace:
    return self.similar(Ops.COS, [self])


def save_graph(G, fn, opt=""):
    print("saving", G, f"to {fn}.svg")
    nx.drawing.nx_pydot.write_dot(G, f'{fn}.dot')
    os.system(f'dot {opt} -Tsvg {fn}.dot -o {fn}.svg')


x = Trace('x')
y = Trace('y')
z = Trace('z')


def fun(x, y, z):
    a1 = x * y
    a2 = cos(a1)
    a3 = a2 / a1
    a4 = a1 - z
    a5 = a3 / a4
    a6 = a5 * 3
    a7 = 2 * a6
    a1.name = "a1"
    a2.name = "a2"
    a3.name = "a3"
    a4.name = "a4"
    a5.name = "a5"
    a6.name = "a6"
    a7.name = "a7"
    return a7


v = fun(x, y, z)
v2 = v.reverse_ad()
save_graph(v.graph()[0], "hi")
save_graph(v2.graph()[0], "hi2")
